# -*- encoding: utf-8 -*-
{
    'name': 'Reporte de Ordenes de compra para Lavoro',
    'category': 'purchase',
    'author': 'ITGRUPO',
    'depends': ['purchase'],
    'version': '1.0',
    'description':"""
    Modulo que agrega un reporte en compras
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'views/report_it_group.xml'
        ],
    'installable': True
}

